import { Component, OnInit } from '@angular/core';
import { interval, Observable } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import * as ons from 'onsenui';

@Component({
    selector: 'app-life-game',
    templateUrl: './life-game.component.html',
    styleUrls: ['./life-game.component.scss'],
})
export class LifeGameComponent implements OnInit {
    field: number[][];
    colsLength: number = 15;
    rowsLength: number = 15;
    generation: number = 0;

    status: 'on' | 'off' = 'off';
    timeDuration = 250;
    auto$: Observable<unknown>;
    generationList: { generation: number; counter: number }[] = [];

    constructor() {}

    ngOnInit(): void {
        this.recreateAuto();
        this.initField();
    }

    initField() {
        this.field = [];
        for (let index = 0; index < +this.rowsLength; index++)
            this.field.push([...new Array(+this.colsLength).fill(0)]);
        this.generation = 1;
        this.generationList.length = 0;
    }

    private cellOnStart: number = -1;
    toggleCell(indexRow: number, indexCol: number) {
        if (this.status === 'off') {
            this.field[indexRow][indexCol] = this.field[indexRow][indexCol] === 1 ? 0 : 1;
            this.cellOnStart = this.field[indexRow][indexCol];
        }
    }

    onMouseDownCell(indexRow: number, indexCol: number) {
        this.toggleCell(indexRow, indexCol);
    }

    onMouseMoveCell(indexRow: number, indexCol: number) {
        if (this.cellOnStart > -1) this.field[indexRow][indexCol] = this.cellOnStart;
    }

    onMouseUpCell() {
        this.cellOnStart = -1;
    }

    nextGen() {
        this.generation++;
        const newField: number[][] = [];
        let counterField = 0;
        for (let indexRow = 0; indexRow < this.field.length; indexRow++) {
            const row = this.field[indexRow];
            const newRow: number[] = [];

            for (let indexCol = 0; indexCol < row.length; indexCol++) {
                const cell = row[indexCol];
                let counter = this.checkNeighbour(indexRow, indexCol);
                if (counter === 3) newRow[indexCol] = 1;
                else if (counter === 2) newRow[indexCol] = this.field[indexRow][indexCol];
                else newRow[indexCol] = 0;

                counterField += newRow[indexCol];
            }
            newField[indexRow] = newRow;
        }
        this.field = newField;

        this.generationList.push({ generation: this.generation, counter: counterField });
    }

    checkNeighbour(indexRow: number, indexCol: number): number {
        const neighbour = {
            xPlus: (indexRow + (this.rowsLength + 1)) % this.rowsLength,
            yPlus: (indexCol + (this.colsLength + 1)) % this.colsLength,
            xMinus: (indexRow + (this.rowsLength - 1)) % this.rowsLength,
            yMinus: (indexCol + (this.colsLength - 1)) % this.colsLength,
        };

        return (
            this.field[neighbour.xMinus][neighbour.yMinus] +
            this.field[neighbour.xMinus][indexCol] +
            this.field[neighbour.xMinus][neighbour.yPlus] +
            this.field[indexRow][neighbour.yMinus] +
            this.field[indexRow][neighbour.yPlus] +
            this.field[neighbour.xPlus][neighbour.yMinus] +
            this.field[neighbour.xPlus][indexCol] +
            this.field[neighbour.xPlus][neighbour.yPlus]
        );
    }

    toggleState() {
        this.status = this.status === 'on' ? 'off' : 'on';

        if (this.status === 'on') {
            this.auto$.subscribe(this.handleAutoSub);
        }
    }

    isGenDead(): boolean {
        let counter = 0;
        for (let index = 0; index < this.field.length; index++) {
            const row = this.field[index];
            counter += row.reduce((x, val) => x + val);
        }
        return counter === 0;
    }

    isLastNGenSame(n: number): boolean {
        let gens = this.generationList.slice(this.generationList.length - n, this.generationList.length);
        return [...new Set(gens.map((x) => x.counter))].length === 1;
    }

    handleAutoSub = (x) => {
        this.nextGen();
        ((this.isGenDead() && this.toast('No cell alive!!!')) ||
            (this.generationList.length >= 10 && this.isLastNGenSame(10) && this.toast('The field is not evolving anymore!!!!'))) &&
            this.toggleState();
    };

    toast(msg: string): boolean {
        ons.notification.toast(msg, { timeout: 2000 });
        return true;
    }

    randomField() {
        this.field = [...this.field.map(row => [...(row.map(cell => Math.round(Math.random())))])];
        this.generation = 1;
    }

    recreateAuto() {
        this.auto$ = interval(this.timeDuration).pipe(takeWhile((x) => this.status === 'on'));
    }

    get alivedCell() {
        return this.field.map(row => row.reduce((val, x) => val + x)).reduce((val, x) => val + x);
    }
}
