import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LifeGameComponent } from './pages/life-game/life-game.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/lifeGame'
  },
  {
    path: 'lifeGame',
    component: LifeGameComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
